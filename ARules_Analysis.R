library("arules")
library("colorspace")
library("ggplot2")
library("dplyr")
library("reshape2")

source("Auxilary.R")

data_complete <- read.csv(file=".\\WorkingData\\Complete_Matrix.csv", header=TRUE)

data_transactions <- as(as.matrix(select(data_complete, -c(ChampID, Frail_Robust, Frail_Prefrail))), "transactions")

most_frequent_patterns <-  eclat(data_transactions, 
                                     parameter=new("ECparameter", 
                                                   tidLists=TRUE, 
                                                   support = 0.07, 
                                                   ext = TRUE, 
                                                   minlen=2, 
                                                   maxlen=ncol(data_complete)
                                                   ), 
                                     control=  new("ECcontrol", verbose = FALSE)
                                     )

#length(most_frequent_patterns)

matrix_N_Cases <- select_and_multiply(conditional_frequent_sets = most_frequent_patterns, 
                                      TF_interestingness = rep(TRUE, length(most_frequent_patterns)),
                                      values = quality(most_frequent_patterns)$transIdenticalToItemsets)  # extract the counts
 
####### Prepare Heatmap for most frequent patterns overall ############
longData <- melt(matrix_N_Cases)
#head(longData, 20)
 

longData$Var2 <- factor(longData$Var2)
longData$Var2 <- make_pretty_labels(longData$Var2)
longData$Var1 <- factor(longData$Var1)
Maxes <- longData %.% group_by(Var1) %.% summarise(RowMaxCases= max(value))
Maxes$Order <- base::order(Maxes$RowMaxCases)
Maxes
Maxes <- Maxes[Maxes$Order,]
Maxes$Y <- 1:nrow(Maxes)

longData <- merge(longData, select(Maxes, -Order) )

greens <- sequential_hcl(2, h = 140, c = c(0, 50), l = c(100, 25), power=1) 


hm_Overall <- make_one_heatmap.gg(longData, xVar="Var2", yVar="Y", fillVar="value",
                        my_colors=greens, 
                        overall_max=505, 
                        this_main="Most Frequent Patterns of Multimorbidity", 
                        this_title="Frequency")


pdf(file=paste0(".\\Output\\", format(Sys.time(), "%Y_%m_%d"), "_Heatmap_of_Most_Frequent_Patterns.pdf"),
    width= 7.5, paper="special")
plot(hm_Overall)
dev.off()


############### Conditional Frequent Set Analysis #########
data_matrix <- as.matrix(select(data_complete, -c(ChampID, Frail_Robust, Frail_Prefrail)))


## set generic eclat parameters
newEC_Param   <- new("ECparameter", 
                     tidLists=TRUE, 
                     support = 1 / nrow(data_matrix), #that's a minimum support of one case
                     ext = TRUE, 
                     maxlen=ncol(data_matrix))

newEC_Control <- new("ECcontrol", verbose = FALSE)

#all_freq_sets <- eclat(data_transactions, parameter=newEC_Param, newEC_Control)
    
transactions_all <- data_transactions

reference_freq_sets <- eclat(transactions_all, newEC_Param, newEC_Control)

# calculation of eclat results 
        ## initiate for first disease
results_tmp <- create_indiv_eclat_summary(matrix_of_all_transactions=data_matrix, 
                                          reference_freq_sets=reference_freq_sets,
                                          col_index= 1,
                                          support_threshold=0.1,
                                          min_lift_a = 1, 
                                          min_lift_b=2, 
                                          abs_support = FALSE,
                                          parameter=newEC_Param, 
                                          control=newEC_Control)

# this creates a data frame with the relevant statistics
results <- results_tmp[[1]]

assign( "tmp", results_tmp[[2]])
List_of_sets <- list( get("tmp"))

assign( "tmp", results_tmp[[3]])
List_of_lifts <- list( get("tmp"))


for (i in 2:ncol(data_matrix)){
        results_tmp <- create_indiv_eclat_summary(matrix_of_all_transactions=data_matrix, reference_freq_sets=reference_freq_sets,
                                                            col_index= i , support_threshold=0.1, 
                                                            min_lift_a = 1, min_lift_b=2, 
                                                            abs_support = FALSE,
                                                            parameter=newEC_Param, control=newEC_Control)
        results <- rbind(results, results_tmp[[1]])
        assign( "tmp", results_tmp[[2]])
        List_of_sets <- c( List_of_sets, get("tmp"))
        
        assign( "tmp", results_tmp[[3]])
        List_of_lifts <- append( List_of_lifts, list(get("tmp")))
        }

names(List_of_sets) <- as.character(results$Disease)
names(List_of_lifts) <- as.character(results$Disease)


results <- transform(results,  percent_lifting_Cases_a = N_lifting_Cases_a/N_Observations, 
                      percent_lifting_Cases_b = N_lifting_Cases_b/N_Observations, 
                      percent_lifting_Cases_a_minSupp = N_lifting_Cases_a_minSupp/N_Observations,
                      percent_lifting_Cases_b_minSupp = N_lifting_Cases_b_minSupp /N_Observations)

write.csv(results, file=paste(".\\Output\\CHAMP_investigate cutoffs", format(Sys.time(), "%Y_%m_%d") ,".csv", sep="_") )                     


round(cor(results[,-1], method="spearman"), 2)


####### Coonditional Heatmaps ##########



pdf(file=paste0(".\\Output\\", format(Sys.time(), "%Y_%m_%d"), "_Heatmaps_Interesting_Sets_Per_Condition_joint.pdf"), width=15, height=7, paper="special")
Index_Diseases <- make_pretty_titles( names(List_of_sets) )

for (i in 1:length(List_of_sets)){
    
interestingness_idx <- List_of_lifts[[i]] > 2 & is.closed(List_of_sets[[i]]) & quality(List_of_sets[[i]])$support > 0.1

matrix_N_Cases <-  select_and_multiply(conditional_frequent_sets = List_of_sets[[i]], 
                                        TF_interestingness = interestingness_idx,
                                        values = quality(List_of_sets[[i]])$transIdenticalToItemsets)  # extract the counts

matrix_Lift <-  select_and_multiply(conditional_frequent_sets = List_of_sets[[i]], 
                                        TF_interestingness = interestingness_idx,
                                        values =  List_of_lifts[[i]] #extract the lifts
                                    )   


greens <- sequential_hcl(2, h = 140, c = c(0, 50), l = c(100, 25), power=1) 
blues <- sequential_hcl(2, h = 230, c = c(0, 50), l = c(100, 25), power=1) 

if (nrow(matrix_N_Cases) >= 1){ make_two_heatmaps.gg(Cases_Matrix=matrix_N_Cases, 
                                                    Lift_Matrix=matrix_Lift, 
                                                    Case_Colours=greens, 
                                                    Case_Max = 130, 
                                                    Lift_Colours = blues, 
                                                    Lift_Max = 17, 
                                                    Colnames_list=Index_Diseases, 
                                                    index=i)
                                } else {print(paste0("No interesting sets found for ", Index_Diseases[i])) 
                                        plot(-1,-1, xlim=c(0,1), ylim=c(0,1), axes=FALSE, frame.plot=FALSE, xlab="", ylab="", 
                                        text(0.5,0.5, paste0("No interesting sets found for ", Index_Diseases[i])) , col="white")
                                        #plot(-1,-1, xlim=c(0,1), ylim=c(0,1), axes=FALSE, frame.plot=FALSE, xlab="", ylab="", bty="n", col="white")
                                        }  
}
dev.off() 




