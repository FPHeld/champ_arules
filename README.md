#Git Repository for the source code to "ASSOCIATION RULES ANALYSIS OF COMORBIDITY: THE CONCORD HEALTH AND AGEING IN MEN PROJECT"#


## Summary ##

This project uses frequent set analysis and association rules to identify patterns of multimorbidity in a cross-sectional data set from the CONCORD HEALTH AND AGEING IN MEN PROJECT at the University of Sydney and the Concord Hospital, Sydney, Australia.

## What is this repository for? ##
This repository makes available the source code of the underlying analysis. It is written in R, importing especially libraries "arules" and "ggplot2".

## How do I get set up? ##
This repo contains four different R scripts

1. Prepare_Data.R prepares the data from the CHAMP study and provides one file, 'Complete_Matrix.csv' for the other scripts. 
1. Auxiliary.R holds various auxiliary functions that are sourced into the other two main scripts
1. Networks_and_Dendrograms.R prepares data for network analysis and additional cluster analysis
1. ARules_Analysis.R is the main analysis script.

To replicate the arules analysis, you will only need the auxiliary functions and the ARules_Analysis.R script. The input format to this script needs to be a binary matrix as csv file with individual diseases in columns and cases in rows.

### Who do I talk to? ###

* Repo owner is Fabian Held at the University of Sydney.

 The code is not yet written for straight forward application to other data sets. There may be bugs and incompatibilities. Please contact me if you are interested in using or adapting the code and run into trouble.