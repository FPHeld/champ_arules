####### Arules


select_and_multiply <- function(conditional_frequent_sets, TF_interestingness, values){
      
        interesting_sets <- subset(conditional_frequent_sets, TF_interestingness)
        out <- matrix(nrow=0, ncol=0)
        if (length(interesting_sets) > 0){
                                         items_in_interesting_sets <- as(items(x=interesting_sets), "matrix")
                                         out <- diag(values[TF_interestingness]) %*% items_in_interesting_sets
                                         }
out
}


create_indiv_eclat_summary <- function(matrix_of_all_transactions, 
                                       reference_freq_sets,
                                       col_index = 1, 
                                       support_threshold = 10, 
                                       min_lift_a = 1, 
                                       min_lift_b = 2,
                                       abs_support = TRUE,
                                       parameter, control){    
        if (support_threshold < 1 & abs_support) stop("support_threshold and abs_support are inconsistent")
    
        Name_Disease_i <- colnames(matrix_of_all_transactions)[col_index]
        
        transactions_all <- as(as.matrix(matrix_of_all_transactions), "transactions")
        
        #select all transactions that contain i, but remove i from the columns
        transactions_no_i <- extract_transactions_conditional_on_i(matrix_of_all_transactions, col_index)
        
        # calculate eclat summary for this subset of transactions only 
        freq_sets_cond_i   <- eclat(transactions_no_i, parameter, control)
        
                summary_i <- summary(freq_sets_cond_i)
                        
                #which rule consequent matches the itemsets in the comprehensive data?
                match_i_in_orig <- match(freq_sets_cond_i, reference_freq_sets)
                        
                        
                ## extract support from the new and from the matched comprehensive data
                Support_of_Selected <- quality(freq_sets_cond_i)[,1]
                Support_of_Originals <- quality(reference_freq_sets[match_i_in_orig])[,1]
                        
                #### #     Assessment Criteria
                        # Are there more than 10 cases of this rule observed !?
     if  (abs_support) { Logical_MinSupport <- quality(freq_sets_cond_i)$transIdenticalToItemsets  > support_threshold }
                ###### or, looking at the percentage: are there more than supp_thres% in the sample
     if (!abs_support) { Logical_MinSupport <- quality(freq_sets_cond_i)$support  > support_threshold }           
                #compute the lift from these 
                Lift_i <- Support_of_Selected / Support_of_Originals
                # Is the Lift above 1 (maybe 2?)
                    Logical_MinLift_a    <- Lift_i > min_lift_a
                    Logical_MinLift_b    <- Lift_i > min_lift_b
        
                N_lifting_Cases_a           <- extract_N_supporting_cases(input_freqsets=freq_sets_cond_i, all_transactions=transactions_no_i, conditions=Logical_MinLift_a)
                N_lifting_Cases_b           <- extract_N_supporting_cases(input_freqsets=freq_sets_cond_i, all_transactions=transactions_no_i, conditions=Logical_MinLift_b)
                N_lifting_Cases_a_minSupp   <- extract_N_supporting_cases(input_freqsets=freq_sets_cond_i, all_transactions=transactions_no_i, conditions=Logical_MinLift_a & Logical_MinSupport)
                N_lifting_Cases_b_minSupp   <- extract_N_supporting_cases(input_freqsets=freq_sets_cond_i, all_transactions=transactions_no_i, conditions=Logical_MinLift_b & Logical_MinSupport)
        
        
        results_tmp <- data.frame(
                              Disease =   Name_Disease_i,
                              N_Observations = summary_i@info$ntransactions, 
                              N_itemsets = summary_i@items@Dim[1],
                              N_MinSupport = sum(Logical_MinSupport),
                              N_MinLift_a = sum(Logical_MinLift_a),
                              N_MinLift_b = sum(Logical_MinLift_b),
                              N_lifting_Cases_a,
                              N_lifting_Cases_b,
                              N_lifting_Cases_a_minSupp,
                              N_lifting_Cases_b_minSupp
                              )
        return(list(results_tmp, freq_sets_cond_i, Lift_i) )
}

## select from a set of transactions those that are positive for column col_index
extract_transactions_conditional_on_i <- function(matrix_of_all_transactions, col_index = 1 ){
                                                    data_no_i <- matrix_of_all_transactions[matrix_of_all_transactions[, col_index] == 1,] 
                                                    data_no_i <- data_no_i[,-col_index]  
                                                    data_no_i <- as(data_no_i, "transactions")
                                                    return(data_no_i)
                                                }

extract_N_supporting_cases <- function(input_freqsets, conditions=TRUE, all_transactions ){
            # find cases in a set of transactions that match a a subset of freqsets which is selected by conditions
            if( !is.logical(conditions )) stop("conditions are not logical")
            
            subset_of_freqsets <- subset(input_freqsets, subset=conditions)
            Ncases <- 0
            if  (length(subset_of_freqsets) > 0) { 
                            Ncases <-  sum(colSums(as(supportingTransactions(subset_of_freqsets, all_transactions), "matrix")) >= 1) 
                            }
            return(Ncases)
}

make_pretty_labels <- function(a_factor){
    levels(a_factor)[levels(a_factor) == "Cong..Heart.Failure"] <- "Cong. Heart Failure"
    levels(a_factor)[levels(a_factor) == "Heart.Attack"] <- "Heart Attack"
    levels(a_factor)[levels(a_factor) == "Periph.Arterial.Disease"] <- "Periph. Arterial Disease"
    levels(a_factor)[levels(a_factor) == "Hearing.Imp."] <- "Hearing Impairment"
    levels(a_factor)[levels(a_factor) == "Back.Pain"] <- "Back Pain"
    levels(a_factor)[levels(a_factor) == "Cog.Impairment"] <- "Cognitive Impairment"
    levels(a_factor)[levels(a_factor) == "Frail"] <- "Frailty"
    a_factor
    }

make_pretty_titles <- function(charVec){
    charVec[charVec == "Cong..Heart.Failure"] <- "Cong. Heart Failure"
    charVec[charVec == "Heart.Attack"] <- "Heart Attack"
    charVec[charVec =="Periph.Arterial.Disease"] <- "Periph. Arterial Disease"
    charVec[charVec == "Hearing.Imp."] <- "Hearing Impairment"
    charVec[charVec =="Back.Pain"] <- "Back Pain"
    charVec[charVec == "Cog.Impairment"] <- "Cognitive Impairment"
    charVec[charVec == "Frail"] <- "Frailty"
    charVec
    }


### Printing 
pal <- function(col, border = "light gray", ...){
     n <- length(col)
     plot(0, 0, type="n", xlim = c(0, 1), ylim = c(0, 1),
     axes = FALSE, xlab = "", ylab = "", ...)
     rect(0:(n-1)/n, 0, 1:n/n, 1, col = col, border = border)
}


# input must be a data frame in long format with column 1 and 2 as factors and column 3 numeric, providing the values for colours (just melt a matrix)
# colors must be a character vector of length 2 containing the scale colors


make_one_heatmap.gg <- function(longData, xVar, yVar, fillVar,
                                my_colors=c("white", "black"), 
                                overall_max=max(longData[[3]]), 
                                this_main="", 
                                this_title=""){

zp1 <- ggplot(longData, aes_string(x = xVar, y = yVar, fill = fillVar))  + 
                geom_tile() + 
    scale_fill_gradient(low = my_colors[[1]], high = my_colors[[2]], limits=c(0, overall_max), na.value = "white" )

#for more than high and low colors
#scale_fill_gradientn(..., colours, values = NULL, space = "Lab",
 # na.value = "grey50", guide = "colourbar")

#zp1 <- zp1  + scale_x_discrete(limits=(x$V2)[order(x$V3)])
#zp1 <- zp1  + scale_y_discrete(limits=(x$V2)[order(x$V3)])
zp1 <- zp1 + theme(panel.grid.minor = element_blank(), 
                   panel.background = element_blank(),
                   axis.text.x = element_text(angle = 45, hjust = 1),
                   axis.text.y = element_blank(),
                   axis.ticks.y= element_blank())
zp1 <- zp1 + labs(x="", y="", fill=this_title)     
zp1 <- zp1 + ggtitle(this_main)
#zp1 <- zp1 + coord_equal()
zp1 <- zp1 + geom_hline(yintercept=seq(from=0.5, to=( max(longData[[yVar]]) + 0.5), by=1), colour="grey", size=0.1, alpha=.3) 
zp1
}




make_two_heatmaps.gg <- function(Cases_Matrix, Lift_Matrix, Case_Colours, Case_Max, Lift_Colours, Lift_Max, Colnames_list, index){

longData_cases <- melt(Cases_Matrix,  value.name="NCases")
longData_Lift <- melt(Lift_Matrix,  value.name="Lift")
longData <- merge(longData_cases, longData_Lift)

longData$Var2 <- factor(longData$Var2)
longData$Var2 <- make_pretty_labels(longData$Var2)   
longData$Var1 <- factor(longData$Var1)
Maxes <- longData %.% group_by(Var1) %.% summarise(RowMaxCases= max(NCases), RowMaxLift=max(Lift))

Maxes$Order <- base::order(Maxes$RowMaxCases, Maxes$RowMaxLift)
#print(max(Maxes$RowMaxLift))
Maxes <- Maxes[Maxes$Order,]
Maxes$Y <- 1:nrow(Maxes)

longData <- merge(longData, select(Maxes, -Order) )


                        p_cases <- make_one_heatmap.gg(longData=longData, xVar="Var2", yVar="Y", fillVar="NCases",
                                                        my_colors=Case_Colours, 
                                                        overall_max=Case_Max,  
                                                        this_main=paste0("Occurrence of frequent sets that include ",  Colnames_list[[index]]),
                                                        this_title="# Cases")

                        p_lift <- make_one_heatmap.gg(longData=longData, xVar="Var2", yVar="Y", fillVar="Lift",
                                                      my_colors=Lift_Colours,   
                                                      overall_max=Lift_Max, 
                                                      this_main=paste0("Lift of frequent sets that include ",  Colnames_list[[index]]), 
                                                      this_title="Lift")
                        multiplot(p_cases, p_lift, cols=2)
}





multiplot <- function(..., plotlist=NULL, file, cols=1, layout=NULL) {
  require(grid)

  # Make a list from the ... arguments and plotlist
  plots <- c(list(...), plotlist)

  numPlots = length(plots)

  # If layout is NULL, then use 'cols' to determine layout
  if (is.null(layout)) {
    # Make the panel
    # ncol: Number of columns of plots
    # nrow: Number of rows needed, calculated from # of cols
    layout <- matrix(seq(1, cols * ceiling(numPlots/cols)),
                    ncol = cols, nrow = ceiling(numPlots/cols))
  }

 if (numPlots==1) {
    print(plots[[1]])

  } else {
    # Set up the page
    grid.newpage()
    pushViewport(viewport(layout = grid.layout(nrow(layout), ncol(layout))))

    # Make each plot, in the correct location
    for (i in 1:numPlots) {
      # Get the i,j matrix positions of the regions that contain this subplot
      matchidx <- as.data.frame(which(layout == i, arr.ind = TRUE))

      print(plots[[i]], vp = viewport(layout.pos.row = matchidx$row,
                                      layout.pos.col = matchidx$col))
    }
  }
}
